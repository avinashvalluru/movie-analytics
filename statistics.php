<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="images/favicon.ico" type="image/ico" />
      <title>Movie - Analytics| </title>
      <!-- Bootstrap -->
      <link href="./vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="./vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- NProgress -->
      <link href="./vendors/nprogress/nprogress.css" rel="stylesheet">
      <!-- iCheck -->
      <link href="./vendors/iCheck/skins/flat/green.css" rel="stylesheet">
      <!-- bootstrap-progressbar -->
      <link href="./vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
      <!-- JQVMap -->
      <link href="./vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
      <!-- bootstrap-daterangepicker -->
      <link href="./vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
      <!-- Custom Theme Style -->
      <link href="./build/css/custom.min.css" rel="stylesheet">
      <link href="./build/css/main.css" rel="stylesheet">
      <!-- jQuery -->
      <script src="./vendors/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="./vendors/bootstrap/dist/js/bootstrap.min.js"></script>
      <script src="./vendors/select-bootstrap.js"></script>
   </head>
   <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            <div class="col-md-3 left_col menu_fixed">
               <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                     <a href="dashboard" class="site_title"><i class="fa fa-file-video-o"></i> <span>Movie- Analytics</span></a>
                  </div>
                  <div class="clearfix"></div>
                  <!-- menu profile quick info -->
                  <div class="profile clearfix">
                     <div class="profile_pic">
                        <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                     </div>
                     <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>Vishnu Manchu</h2>
                     </div>
                  </div>
                  <!-- /menu profile quick info -->
                  <br />
                  <!-- sidebar menu -->
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                     <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                           <li><a href="dashboard"><i class="fa fa-home"></i>Dashboard</a></li>
                           <li><a href="predictions"><i class="fa fa-line-chart"></i>Predictions</a></li>
                           <li><a href="statistics"><i class="fa fa-bar-chart"></i>Statistics</a></li>
                           <li><a href="trending-news"><i class="fa fa-newspaper-o"></i>Trending News</a></li>
                           <li><a href="youtube"><i class="fa fa-youtube-play"></i>Youtube</a></li>
                        </ul>
                     </div>
                  </div>
                  <!-- /sidebar menu -->
               </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
               <div class="nav_menu">
                  <nav>
                     <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                     </div>
                     <ul class="nav navbar-nav navbar-right">
                        <li class="">
                           <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           <img src="images/img.jpg" alt="">Vishnu Manchu
                           <span class=" fa fa-angle-down"></span>
                           </a>
                           <ul class="dropdown-menu dropdown-usermenu pull-right">
                              <li><a href="javascript:;"> Profile</a></li>
                              <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                           </ul>
                        </li>
                     </ul>
                  </nav>
               </div>
            </div>
            <!-- /top navigation -->
            <!-- page content -->
            <div class="right_col" role="main">
               <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_title">
                              <h2>Hero Statistics<small></small></h2>
                              <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
                              <br />
                              <form id="hero" data-parsley-validate class="form-horizontal form-label-left" method="POST" >
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Select Hero<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <select class="selectpicker form-control" id="hero-name" name="hero" required="true" data-live-search="true">
                                          <option value="">Choose option</option>
                                          <?php
                                             $data = file_get_contents("./hero_list.json");
                                             //var_dump($data);
                                             $json=json_decode($data,true);
                                              //var_dump($json);
                                             foreach ($json as $key => $value) {

                                               echo "<option value=\"".$value['HERO']."\">$value[HERO]</option>\n";

                                             }
                                               ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                       <button type="button" id = "hero_view-btn" class="btn btn-success">view</button>
                                    </div>
                                 </div>
                              </form>
                              <div class="loader hide">Loading...</div>

                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-xs-12 widget widget_tally_box ">
                        <div class="x_panel result hide">
                           <div class="x_content">
                              <div class="flex">
                                 <ul class="list-inline widget_profile_box">
                                    <li>
                                       <a>
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </li>
                                    <li>
                                       <img src="images/user.png" alt="..." class="img-circle profile_img">
                                    </li>
                                    <li>
                                       <a>
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                              <h3 class="heroName text-center"></h3>
                              <div class="flex">
                                 <ul class="list-inline count2">
                                    <li>
                                       <span><i class="fa fa-facebook fa-1x"></i></span>
                                        <h5 id="fblikes"></h5>
                                    </li>
                                    <li>
                                      <span><i class="fa fa-twitter fa-1x"></i></span>
                                       <h5 id = "twitter"></h5>
                                    </li>
                                    <li>
                                      <span><i class="fa fa-instagram fa-1x"></i></span>
                                       <h5 id = "instagram"></h5>
                                    </li>
                                 </ul>
                              </div>
                              <p>
                                 <!-- tweak all of this a bit, there are few things you should do. -->
                              </p>
                           </div>
                        </div>
                     </div>
                   </div>

                         <div class="row">
                           <!-- <div class="col-md-5 col-sm-12 col-xs-12">
                                     <div class="x_panel result hide">
                                        <div class="x_title">
                                           <h2>Hero Stats</h2>
                                           <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                           <canvas id="herobarChart"></canvas>
                                        </div>
                                     </div>
                                  </div> -->
                                  <div class="col-md-6 col-sm-4 col-xs-12">
                                                <div class="x_panel tile fixed_height_320 overflow_hidden result hide">
                                                  <div class="x_title ">
                                                    <h2>Hero Stats</h2>
                                                    <div class="clearfix"></div>
                                                  </div>
                                                  <div class="x_content">
                                                    <table class="" style="width:100%">

                                                      <tr>
                                                        <td id="donutCanvas">

                                                        </td>
                                                        <td>
                                                          <table class="tile_info">
                                                            <tr>
                                                              <td>
                                                                <p><i class="fa fa-square aero"></i>Block Buster</p>
                                                              </td>
                                                              <td id="bb"></td>
                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                <p><i class="fa fa-square red"></i>Hit</p>
                                                              </td>
                                                              <td id="hit"></td>
                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                <p><i class="fa fa-square green"></i>Average</p>
                                                              </td>
                                                              <td id="avg"></td>
                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                <p><i class="fa fa-square blue"></i>Below Average</p>
                                                              </td>
                                                              <td id="bavg"></td>
                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                <p><i class="fa fa-square purple"></i>Flop</p>
                                                              </td>
                                                              <td id="flop"></td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                     <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="x_panel result hide">
                                           <div class="x_title">
                                              <h2>Collections</h2>
                                              <div class="clearfix"></div>
                                           </div>
                                           <div class="x_content" id="barchartCanvas">

                                           </div>
                                        </div>
                                     </div>

                         </div>

                  </div>
                  <div class="row">


                              <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="x_panel result hide">
                                    <div class="x_title">
                                       <h2>Movie List</h2>
                                       <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                       <p class="text-muted font-13 m-b-30">
                                          <!-- Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table. -->
                                       </p>
                                       <table id="moviedatatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                          <thead>
                                             <tr>
                                                <th>Title</th>
                                                <th>Director</th>
                                                <th>Verdict</th>
                                                <th>Gross</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                  </div>

               </div>
            </div>
            <!-- /page content -->
            <!-- footer content -->
            <footer>
               <div class="pull-right">
                  Movie-Analytics by <a href="#">SVEI</a>
               </div>
               <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
         </div>
      </div>
      
      <!-- FastClick -->
      <script src="./vendors/fastclick/lib/fastclick.js"></script>
      <!-- NProgress -->
      <script src="./vendors/nprogress/nprogress.js"></script>
      <!-- Chart.js -->
      <script src="./vendors/Chart.js/dist/Chart.min.js"></script>
      <!-- gauge.js -->
      <script src="./vendors/gauge.js/dist/gauge.min.js"></script>
      <!-- bootstrap-progressbar -->
      <script src="./vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
      <!-- iCheck -->
      <script src="./vendors/iCheck/icheck.min.js"></script>
      <!-- Skycons -->
      <script src="./vendors/skycons/skycons.js"></script>
      <!-- Flot -->
      <script src="./vendors/Flot/jquery.flot.js"></script>
      <script src="./vendors/Flot/jquery.flot.pie.js"></script>
      <script src="./vendors/Flot/jquery.flot.time.js"></script>
      <script src="./vendors/Flot/jquery.flot.stack.js"></script>
      <script src="./vendors/Flot/jquery.flot.resize.js"></script>
      <!-- Flot plugins -->
      <script src="./vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
      <script src="./vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
      <script src="./vendors/flot.curvedlines/curvedLines.js"></script>
      <!-- DateJS -->
      <script src="./vendors/DateJS/build/date.js"></script>
      <!-- JQVMap -->
      <script src="./vendors/jqvmap/dist/jquery.vmap.js"></script>
      <script src="./vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
      <script src="./vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
      <!-- bootstrap-daterangepicker -->
      <script src="./vendors/moment/min/moment.min.js"></script>
      <script src="./vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
      <script src="./vendors/datatables.net/js/jquery.dataTables.min.js"></script>
      <script src="./vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
      <script src="./vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
      <script src="./vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
      <script src="./vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
      <script src="./vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
      <script src="./vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
      <script src="./vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
      <script src="./vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
      <script src="./vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
      <script src="./vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
      <script src="./vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
      <!-- Custom Theme Scripts -->
      <script src="./build/js/custom.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function(){
             $('#moviedatatable').DataTable({
                   "lengthMenu": [
                       [10, 25, 50],
                       [10, 25, 50]
                   ],
                   language: {
                       search: "_INPUT_",
                       searchPlaceholder: "Search records",
                   }

               });
         });

      </script>
      <script type="text/javascript">
         $("#hero_view-btn").click(function(){

            var heroName = $("#hero-name").val();
            //console.log(hero);
            if(heroName)
            {
              //alert(hero);
              $.ajax({
                  type:"POST",
                  url:"statistics_s",
                  data: { hero :heroName },
                  beforeSend: function(){
                        // $('.ajax-loader-wrapper').show();

                         $(".loader").removeClass("hide");
                    },
                    complete: function(){
                        // $('.ajax-loader-wrapper').hide();
                         $(".loader").addClass("hide");
                    },
                  success: function(response){
                  //$("#analysis-result").html(response);
                  var res = JSON.parse(response);
                  console.log(res);
                    // $('.bar-graph').removeClass("hide");
                    // var rating = res.Rating
                    // rating = (rating/5)*100;
                    // $('#analysis-result').html('<h5>Predicted rating</h5><div class="star-ratings-sprite"><span style="width:'+rating+'%" class="star-ratings-sprite-rating"></span></div>')
                      $('.result').removeClass("hide");

                      $('#fblikes').html(res['social_media']['Fb_likes']);
                      $('#twitter').html(res['social_media']['Twitter_followers']);
                      $('#instagram').html(res['social_media']['Insta_followers']);

                      $('.heroName').html(heroName);
                                  $("#donutCanvas").html('<canvas id="verdictDoughnut" height="180" width="135" style="margin: 15px 10px 10px 0"></canvas>')
                                  if ($('#verdictDoughnut').length){

                                  var chart_doughnut_settings = {
                                      type: 'doughnut',
                                      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                                      data: {
                                        labels: [
                                          "Block Buster",
                                          "Hit",
                                          "Average",
                                          "Below Average",
                                          "Flop"
                                        ],
                                        datasets: [{
                                          data: [res['verdict']['BLOCK BUSTER'], res['verdict']['HIT'],res['verdict']['AVERAGE'],res['verdict']['BELOW AVERAGE'], res['verdict']['FLOP']],
                                          backgroundColor: [
                                            "#BDC3C7",
                                            "#E74C3C",
                                            "#26B99A",
                                            "#3498DB",
                                            "#9B59B6"
                                          ],
                                          hoverBackgroundColor: [
                                            "#CFD4D8",
                                            "#E95E4F",
                                            "#36CAAB",
                                            "#49A9EA",
                                            "#B370CF"
                                          ]
                                        }]
                                      },
                                      options: {
                                        legend: false,
                                        responsive: false
                                      }
                                    }

                                    $('#verdictDoughnut').each(function(){

                                      var chart_element = $(this);
                                      var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);

                                    });

                                  }
                                  $('#barchartCanvas').html('<canvas id="collectionbarChart"></canvas>');
                                  if ($('#collectionbarChart').length ){

                                      var ctx = document.getElementById("collectionbarChart");
                                      var mycollectionbarChart = new Chart(ctx, {
                                      type: 'bar',
                                      data: {
                                        labels: ["NIZAM","ELR","GNT","VZA","VSP","CEEDED","RJY","NLR"],
                                        datasets: [{

                                        backgroundColor: "#26B99A",
                                        data: [res['Region_Collections']['NIZAM'],res['Region_Collections']['ELR'],res['Region_Collections']['GNT'],res['Region_Collections']['VZA'],res['Region_Collections']['VSP'],res['Region_Collections']['CEEDED'],res['Region_Collections']['RJY'],res['Region_Collections']['NLR']]
                                        }]
                                      },

                                      options: {
                                        scales: {
                                        yAxes: [{
                                          ticks: {
                                          beginAtZero: true,

                                          }
                                        }],
                                        xAxes: [{
                                          // categoryPercentage: 1.0,
                                          // barPercentage: 0.3

                                        }]
                                        }
                                      }
                                      });

                                    }
                                  var table = $('#moviedatatable').DataTable();
                                  table.rows().remove().draw();
                                  jQuery.each( res['data'], function( i, val ) {
                                    table.row.add( [
                                      val['TITLE'],
                                      val['DIRECTOR'],
                                      val['VERDICT'],
                                      val['GROSS']

                                  ] ).draw(true);
                                });

                  }
                });
            }else{
              alert("Select hero");
            }
         });
      </script>
      <script type="text/javascript">

      </script>
   </body>
</html>
