<?php

//https://www.youtube.com/watch?v=z05Ft6RBMXI
$url = $_POST['url'];
//$url="https://www.youtube.com/watch?v=cGtOLI4B94c";
//$url="https://www.youtube.com/embed/7_lcte0gnGU";
//$url="https://www.youtube.com/watch?v=-uB9w6crwi8";
//$url="dsasda";
$input = "/var/www/html/movie-analytics/python/youtube_comment_analysis.py --youtube_url $url 2>&1";
//echo "$input";
$output = shell_exec($input);
//var_dump($output);
$jsonData=json_decode($output,true);
//var_dump($jsonData);
if(isset($jsonData['error']))
{
   echo '<div class="text-center" style="color:red"><h3>'.$jsonData['error'].'</h3></div>';

}
else {

  echo '<div class="col-md-8 col-sm-4 col-xs-12">
     <div class="x_panel tile fixed_height_320">
        <div class="x_title">
           <h2>Results</h2>
           <div class="clearfix"></div>
        </div>
        <div class="x_content">
           <h4>Public Trend</h4>
           <br>
           <div class="widget_summary">
              <div class="w_left w_25">
                 <span>Positive</span>
              </div>
              <div class="w_center w_55">
                 <div class="progress">
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:'.$jsonData['pos'] *100 .'%;" >
                    </div>
                 </div>
              </div>
              <div class="w_right w_20">
                 <span>'.$jsonData['pos'] *100 .'%</span>
              </div>
              <div class="clearfix"></div>
           </div>
           <div class="widget_summary">
              <div class="w_left w_25">
                 <span>Negative</span>
              </div>
              <div class="w_center w_55">
                 <div class="progress">
                    <div class="progress-bar bg-red" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:'.$jsonData['neg'] *100 .'%;" >
                    </div>
                 </div>
              </div>
              <div class="w_right w_20">
                 <span>'.$jsonData['neg'] *100 .'%</span>
              </div>
              <div class="clearfix"></div>
           </div>
           <div class="widget_summary">
              <div class="w_left w_25">
                 <span>Neutral</span>
              </div>
              <div class="w_center w_55">
                 <div class="progress">
                    <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:'.$jsonData['neu'] *100 .'%;" >
                    </div>
                 </div>
              </div>
              <div class="w_right w_20">
                 <span>'.$jsonData['neu'] *100 .'%</span>
              </div>
              <div class="clearfix"></div>
           </div>
           <div class="widget_summary">
              <div class="w_left w_25">
                 <span>Compound</span>
              </div>
              <div class="w_center w_55">
                 <div class="progress">
                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:'.$jsonData['compound'] *100 .'%;" >
                    </div>
                 </div>
              </div>
              <div class="w_right w_20">
                 <span>'.$jsonData['compound'] *100 .'%</span>
              </div>
              <div class="clearfix"></div>
           </div>
        </div>
     </div>
  </div>';
}

 ?>
