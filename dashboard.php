<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Movie - Analytics| </title>

    <!-- Bootstrap -->
    <link href="./vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="./vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="./vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="./vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="./vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="./vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="./vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="./build/css/custom.min.css" rel="stylesheet">

    <link href="./build/css/main.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-file-video-o"></i> <span>Movie- Analytics</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Vishnu Manchu</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="dashboard"><i class="fa fa-home"></i>Dashboard</a></li>
                  <li><a href="predictions"><i class="fa fa-line-chart"></i>Predictions</a></li>
                    <li><a href="statistics"><i class="fa fa-bar-chart"></i>Statistics</a></li>
                  <li><a href="trending-news"><i class="fa fa-newspaper-o"></i>Trending News</a></li>
                  <li><a href="youtube"><i class="fa fa-youtube-play"></i>Youtube</a></li>

                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">Vishnu Manchu
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Tollywood Age</span>
              <div class="count">88 Yrs</div>
              <!-- <span class="count_bottom"><i class="green">4% </i> # Screens </span> -->
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Avg. Releases /Yr</span>
              <div class="count">200</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i># Screens</span>
              <div class="count">1399</div>
              <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i># Distribution Territories</span>
              <div class="count">08</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>
          <!-- /top tiles -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Movie-Releases</h3>
                  </div>
                  <div class="col-md-3">
                    <select class="reportYear selectpicker form-control" name="reportYear">
                      <option value="2018" selected>2018</option>
                      <option value="2017">2017</option>
                      <option value="2016">2016</option>
                      <option value="2015">2015</option>
                      <option value="2014">2014</option>
                      <option value="2013">2013</option>
                      <option value="2012">2012</option>
                      <option value="2011">2011</option>
                      <option value="2010">2010</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-8 col-sm-9 col-xs-12" id="movie-chart-canvas">

                </div>
                <div class="col-md-4 col-sm-3 col-xs-12 bg-white">
                  <div class="col-md-12 col-sm-12 col-xs-12" id="verdict-chart-canvas">
                  </div>
                </div>
                <div class="loader hide">Loading...</div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>

          <br />

          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2># Screens</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id="theater_pie" style="height:350px;"></div>

                  </div>
                </div>
              </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
               <div class="x_panel result">
                  <div class="x_title">
                     <h2>Gross</h2>
                     <div class="clearfix"></div>
                  </div>
                  <div class="x_content" id="grossbarChartCanvas">

                  </div>
               </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Movie-Analytics by <a href="#">SVEI</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
</div>
    <!-- jQuery -->
    <script src="./vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="./vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="./vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="./vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="./vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="./vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="./vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="./vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="./vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="./vendors/Flot/jquery.flot.js"></script>
    <script src="./vendors/Flot/jquery.flot.pie.js"></script>
    <script src="./vendors/Flot/jquery.flot.time.js"></script>
    <script src="./vendors/Flot/jquery.flot.stack.js"></script>
    <script src="./vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="./vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="./vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="./vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="./vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="./vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="./vendors/jqvmap/dist/maps/jquery.vmap.usa.js"></script>

    <script src="./vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="./vendors/moment/min/moment.min.js"></script>
    <script src="./vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="./vendors/raphael/raphael.min.js"></script>
     <script src="./vendors/morris.js/morris.min.js"></script>
     <script src="./vendors/echarts/dist/echarts.min.js"></script>
    <script src="./vendors/echarts/map/js/world.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="./build/js/custom.min.js"></script>
    <script type="text/javascript">



    function displayMovieData(year)
    {
      $.ajax({
          type:"POST",
          url:"dashboard_s.php",
          data: { Movieyear :year},
          beforeSend: function(){

                 $(".loader").removeClass("hide");
            },
            complete: function(){

                 $(".loader").addClass("hide");
            },
          success: function(response){
            var res = JSON.parse(response);
            console.log(res);
            $('#movie-chart-canvas').html('<canvas id="releaseChart"></canvas>');
            $('#verdict-chart-canvas').html('  <div id="verdict_donut" ></div>')
            $('#grossbarChartCanvas').html('<canvas id="grossbarChart"></canvas>')
            if ($('#releaseChart').length ){

                  var ctx = document.getElementById("releaseChart");
                  var lineChart = new Chart(ctx, {
                  type: 'line',
                  data: {
                    labels: [res[0]['Month'], res[1]['Month'], res[2]['Month'], res[3]['Month'], res[4]['Month'], res[5]['Month'], res[6]['Month'], res[7]['Month'], res[8]['Month'], res[9]['Month'], res[10]['Month'], res[11]['Month']],
                    datasets: [{
                    label: "Movie-Releases",
                    backgroundColor: "rgba(38, 185, 154, 0.31)",
                    borderColor: "rgba(38, 185, 154, 0.7)",
                    pointBorderColor: "rgba(38, 185, 154, 0.7)",
                    pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointBorderWidth: 1,
                    data: [res[0]['Movies_count'], res[1]['Movies_count'], res[2]['Movies_count'], res[3]['Movies_count'], res[4]['Movies_count'], res[5]['Movies_count'], res[6]['Movies_count'], res[7]['Movies_count'], res[8]['Movies_count'], res[9]['Movies_count'], res[10]['Movies_count'], res[11]['Movies_count']]
                    }]
                  },
                  options: {
                     legend: {
                        display: false
                     }
                 }
              });
            }
          if ($('#verdict_donut').length ){
              Morris.Donut({
                element: 'verdict_donut',
                data: [
                {label: 'BlockBuster', value: res[12]['Block buster'] },
                {label: 'Hit', value: res[12]['Hit'] },
                {label: 'Average', value: res[12]['Average'] },
                {label: 'Below Average', value: res[12]['Below Average'] },
                {label: 'Flop', value: res[12]['Flop'] }
                ],
                colors: ['#7EFB02', '#0A8E46', '#E1EF0C', '#E18F09', '#EA1D0C'],
                // formatter: function (y) {
                // return y + "%";
                // },
                resize: true
              });
            }

                  if ($('#grossbarChart').length ){

                      var ctx = document.getElementById("grossbarChart");
                      var mycollectionbarChart = new Chart(ctx, {
                      type: 'bar',
                      data: {
                        labels: ["NIZAM","ELR","GNT","VZA","VSP","CEEDED","RJY","NLR"],
                        datasets: [{
                        label : 'Gross(cr)',
                        backgroundColor: "#26B99A",
                        data: [res[13]['NIZAM'], res[13]['ELR'], res[13]['GNT'], res[13]['VZA'], res[13]['VSP'], res[13]['CEEDED'], res[13]['RJY'], res[13]['NLR']]

                        }]
                      },

                      options: {
                        scales: {
                        yAxes: [{
                          ticks: {
                          beginAtZero: true,

                          }
                        }],
                        xAxes: [{
                          // categoryPercentage: 1.0,
                          // barPercentage: 0.3

                        }]
                        }
                      }
                      });

                    }

          }

        });
    }


    $(document).ready(function(){

        var year= $('.reportYear').val();
        displayMovieData(year);
        $('select').on('change', function() {
        displayMovieData(this.value);
        });

        if ($('#theater_pie').length ){

          var echartPie = echarts.init(document.getElementById('theater_pie'));

          echartPie.setOption({
          tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
          },
          // legend: {
          //   x: 'center',
          //   y: 'bottom',
          //   data: ["NIZAM","ELR","GNT","VZA","VSP","CEEDED","RJY","NLR"],
          // },
          toolbox: {
            show: true,
            feature: {
            magicType: {
              show: true,
              type: ['pie', 'funnel'],
              option: {
              funnel: {
                x: '25%',
                width: '50%',
                funnelAlign: 'left',
                max: 1548
              }
              }
            },
            // restore: {
            //   show: true,
            //   title: "Restore"
            // },
            // saveAsImage: {
            //   show: true,
            //   title: "Save Image"
            // }
            }
          },
          calculable: true,
          series: [{
            name: 'Theater List',
            type: 'pie',
            radius: '55%',
            center: ['50%', '48%'],
            data: [{
            value: 257,
            name: 'NIZAM'
            }, {
            value: 95,
            name: 'ELR'
            }, {
            value: 274,
            name: 'GNT'
            }, {
            value: 98,
            name: 'VZA'
            }, {
            value: 108,
            name: 'VSP'
           }, {
           value: 388,
           name: 'CEEDED'
           }, {
           value: 133,
           name: 'RJY'
          },{
           value: 45,
           name: 'NLR'
           }]
          }]
          });

          var dataStyle = {
          normal: {
            label: {
            show: false
            },
            labelLine: {
            show: false
            }
          }
          };

          var placeHolderStyle = {
          normal: {
            color: 'rgba(0,0,0,0)',
            label: {
            show: false
            },
            labelLine: {
            show: false
            }
          },
          emphasis: {
            color: 'rgba(0,0,0,0)'
          }
          };

        }

    });


    </script>

  </body>
</html>
