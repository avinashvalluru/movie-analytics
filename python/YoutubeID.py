#!/usr/bin/python3.5
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
import requests, re, json


links, ids = [], []
url = 'https://www.tollywood.net/videos/trailers/'
response = requests.get(url)
soup = bs(response.text, 'lxml')


for i in soup.findAll(class_='entry-title td-module-title'):
    for a in i.find_all('a', href=True):
        links.append(a['href'])
links = links[:15]
for i in links:
    response = requests.get(i)
    soup = bs(response.text, 'lxml')
    for a in soup.find_all('iframe', src=True):
        ids.append(a['src'][30:][:11])
print(json.dumps(ids))
