#!/usr/bin/python3.5 -W ignore
from sys import argv
import pickle
import argparse
import spacy
import json
parser = argparse.ArgumentParser(description='Movie_rating prediction from plot')
parser.add_argument('--Plot', help='Enter movie plot for analysis.')
args = parser.parse_args()
rating_model = pickle.load(open('./python/models/TOI_RATING.pkl', 'rb'))
budget_model = pickle.load(open('./python/models/TOI_BUDGET.pkl', 'rb'))
gross_model = pickle.load(open('./python/models/TOI_GROSS.pkl', 'rb'))
nlp = spacy.load('en')
if len(argv) > 1:
    plot_text = argv[2]
    plot = nlp(plot_text)
    rating = float(rating_model.predict([plot.vector]))
    budget = float(budget_model.predict([plot.vector]))
    gross = float(gross_model.predict([plot.vector]))
    obj = {'Rating' : rating, 'Budget' : budget, 'Gross' : gross}
    print(json.dumps(obj))
else:
    print("Enter the movie_plot using arguments --plot 'YOUR_PLOT_HERE'. ")
