#!/usr/bin/python3.5
from __future__ import print_function

import os,sys,time,json
import requests
import argparse
import lxml.html
import newlinejson as nlj
import spacy
from bs4 import BeautifulSoup
from lxml.cssselect import CSSSelector
from spacy.tokens import Doc
from nltk.sentiment.vader import SentimentIntensityAnalyzer


YOUTUBE_COMMENTS_URL = 'https://www.youtube.com/all_comments?v={youtube_id}'
YOUTUBE_COMMENTS_AJAX_URL = 'https://www.youtube.com/comment_ajax'

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'

sentiment_analyzer = SentimentIntensityAnalyzer()

def find_value(html, key, num_chars=2):
    pos_begin = html.find(key) + len(key) + num_chars
    pos_end = html.find('"', pos_begin)
    return html[pos_begin: pos_end]


def extract_comments(html):
    tree = lxml.html.fromstring(html)
    item_sel = CSSSelector('.comment-item')
    text_sel = CSSSelector('.comment-text-content')
    time_sel = CSSSelector('.time')
    author_sel = CSSSelector('.user-name')

    for item in item_sel(tree):
        yield {'cid': item.get('data-cid'),
               'text': text_sel(item)[0].text_content(),
               'time': time_sel(item)[0].text_content().strip(),
               'author': author_sel(item)[0].text_content()}


def extract_reply_cids(html):
    tree = lxml.html.fromstring(html)
    sel = CSSSelector('.comment-replies-header > .load-comments')
    return [i.get('data-cid') for i in sel(tree)]


def ajax_request(session, url, params, data, retries=10, sleep=20):
    for _ in range(retries):
        response = session.post(url, params=params, data=data)
        if response.status_code == 200:
            response_dict = json.loads(response.text)
            return response_dict.get('page_token', None), response_dict['html_content']
        else:
            time.sleep(sleep)


def download_comments(youtube_id, sleep=1):
    session = requests.Session()
    session.headers['User-Agent'] = USER_AGENT

    # Get Youtube page with initial comments
    response = session.get(YOUTUBE_COMMENTS_URL.format(youtube_id=youtube_id))
    html = response.text
    reply_cids = extract_reply_cids(html)

    ret_cids = []
    for comment in extract_comments(html):
        ret_cids.append(comment['cid'])
        yield comment

    page_token = find_value(html, 'data-token')
    session_token = find_value(html, 'XSRF_TOKEN', 4)

    first_iteration = True

    # Get remaining comments (the same as pressing the 'Show more' button)
    while page_token:
        data = {'video_id': youtube_id,
                'session_token': session_token}

        params = {'action_load_comments': 1,
                  'order_by_time': True,
                  'filter': youtube_id}

        if first_iteration:
            params['order_menu'] = True
        else:
            data['page_token'] = page_token

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        page_token, html = response

        reply_cids += extract_reply_cids(html)
        for comment in extract_comments(html):
            if comment['cid'] not in ret_cids:
                ret_cids.append(comment['cid'])
                yield comment

        first_iteration = False
        time.sleep(sleep)

    # Get replies (the same as pressing the 'View all X replies' link)
    for cid in reply_cids:
        data = {'comment_id': cid,
                'video_id': youtube_id,
                'can_reply': 1,
                'session_token': session_token}

        params = {'action_load_replies': 1,
                  'order_by_time': True,
                  'filter': youtube_id,
                  'tab': 'inbox'}

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        _, html = response

        for comment in extract_comments(html):
            if comment['cid'] not in ret_cids:
                ret_cids.append(comment['cid'])
                yield comment
        time.sleep(sleep)


def main(argv):
    parser = argparse.ArgumentParser(add_help=False, description=('Download Youtube comments without using the Youtube API'))
    parser.add_argument('--help', '-h', action='help', default=argparse.SUPPRESS, help='Show this help message and exit')
    parser.add_argument('--youtube_url', '-y', help='URL of Youtube video for which to download the comments')
    args = parser.parse_args(argv)
    url = args.youtube_url
    if ytCommentConfirm(url):
        try:
            youtube_id = ''.join(url[-11:])
            output = "./python/outputs/"+youtube_id + '.json'

            if not youtube_id:
                parser.print_usage()
                raise ValueError('you need to specify a Youtube ID')

#            print('Downloading Youtube comments for video:', youtube_id)
            count = 0
            with open(output, 'w') as fp:
                for comment in download_comments(youtube_id):
                    print(json.dumps(comment), file=fp)
                    count += 1
#                    sys.stdout.write('Downloaded %d comment(s)\r' % count)
#                    sys.stdout.flush()
            if count:
                print('')
            else:
                print('\nSorry no comments for analysis!')

        except Exception as e:
            print('Error:', str(e))
            sys.exit(1)
    else:
        print('')

    pwd = os.getcwd()

    return output, pwd

def ytCommentConfirm(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text,'html.parser')
    tag = soup.find('div', class_ = 'display-message vve-check')
    check = False
    if tag is None:
        check = True
    else:
        check = False
    return check

def polarity_scores(doc):
    return sentiment_analyzer.polarity_scores(doc.text)

def comment_analyzer(file):
    polarity_scores_list = []
    
    neu_sum, neg_sum, compound_sum, pos_sum, count = 0,0,0,0,0
    # Add force value to rewrite polarity scores
    Doc.set_extension('polarity_scores', getter=polarity_scores, force = True)
    # Load language, SpaCy supports multi-language
    nlp = spacy.load('en')
    # open JSON file to retrive comments
    with nlj.open(file) as src:
        with nlj.open('./python/outputs/out.json', 'w') as dst:
            for line in src:
                dst.write(line)
                doc = nlp(line['text'])
                polarity_scores_list.append(doc._.polarity_scores)
                count += 1

    for score in polarity_scores_list:
        neu_sum = neu_sum + score['neu']
        neg_sum = neg_sum + score['neg']
        compound_sum = compound_sum + score['compound']
        pos_sum = pos_sum + score['pos']

    if count:
        final_scores = json.dumps({"neu" : round(neu_sum / count, 2), "neg" : round(neg_sum / count, 2), "compound" : round(compound_sum / count, 2), "pos" : round(pos_sum / count, 2)})


    return final_scores

if __name__ == '__main__':
    try:
        file_name, curr_dir = main(sys.argv[1:])
        print(comment_analyzer(file_name))
    except:
        error = json.dumps({"error" : "No Comments For Analysis"})
        print(error)
