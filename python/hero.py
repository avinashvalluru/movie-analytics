#!/usr/bin/python3.5 -W ignore

import csv
import json
import pandas as pd
import argparse
from sys import argv
parser = argparse.ArgumentParser(description = 'Hero Statistics')
parser.add_argument('--Hero', help='Enter Hero Name')
args = parser.parse_args()
if (len(argv)>1):
    hero = argv[2]
df = pd.read_csv('./python/hero_details.csv')
filtered = df['HERO'] == hero
g = df[filtered].groupby('VERDICT').count()
mv_list = []
for i in df[filtered].index:
    mv_dict = {}
    for column in df[filtered].columns:
        mv_dict[column] = df[filtered][column][i]
    mv_list.append(mv_dict)
vd_list = g["TITLE"].to_dict()
k = sorted([i for i in vd_list.keys()])
flist = ['AVERAGE', 'BELOW AVERAGE', 'BLOCK BUSTER', 'FLOP', 'HIT']
dicc = {}
for i in flist:
    if i not in k:
        vd_list.update({i : 0})
jsonobj = {'verdict':vd_list, 'data' : mv_list}
print(json.dumps(jsonobj))
