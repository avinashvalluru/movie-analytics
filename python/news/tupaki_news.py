#!/usr/bin/python3.5 -W ignore

from newsplease import NewsPlease
from bs4 import BeautifulSoup
import requests
import json
import pandas as pd


df = pd.DataFrame()
news_titles, news_imgs, news_urls, news_content = [], [], [], []
url = 'http://english.tupaki.com/movienews'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')

# Extract news titles
for item in range(len(soup.findAll(class_="name"))):
    news_titles.append(soup.findAll(class_="name")[item].text.strip())

# Extract news images
for lk in soup.findAll('ul'):
    for a in lk.find_all('img', src=True):
        link = a['src']
        if 'http://content.tupaki.com' in link:
            news_imgs.append(link)

# Extract news urls
for lk in soup.findAll('ul'):
    for a in lk.find_all('a', href=True):
        link = a['href']
        if 'http://english.tupaki.com/movienews/article/' in link:
            news_urls.append(link)

# Extract news content
for item in news_urls:
    article = NewsPlease.from_url(item)
    news_content.append(article.text)

df['Title'] = news_titles
df['Images'] = news_imgs
df['Content'] = news_content
df['Url'] = news_urls


# Print Tupaki information
obj = df.to_json(orient = "records")
with open("tupaki_news.json", "w+") as output_file:
    output_file.write(obj)
