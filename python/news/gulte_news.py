#!/usr/bin/python3.5 -W ignore

from newsplease import NewsPlease
from bs4 import BeautifulSoup
import requests
import pandas as pd

urls, ti, dt, iu, au, c = [], [], [], [], [], []
df = pd.DataFrame()
url = 'http://www.gulte.com/movienews'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'lxml')


# Collect urls
for lk in soup.findAll(class_='txt'):
    for a in lk.find_all('a', href=True):
        urls.append(a['href'])


# Collect Info from Url
for item in urls:
    article = NewsPlease.from_url(item)
    if article.title:
        ti.append(article.title)
    else:
        ti.append(None)


    if article.date_publish:
        dt.append(str(article.date_publish))
    else:
        dt.append(None)


    if article.image_url:
        iu.append(article.image_url)
    else:
        iu.append(None)


    if article.authors:
        au.append(article.authors)
    else:
        au.append(None)


    if article.text:
        c.append(article.text)
    else:
        c.append(None)

df['Title'] = ti
df['Publish_date'] = dt
df['Image'] = iu
df['Author'] = au
df['Content'] = c


# Print 123telugu information
obj = df.to_json(orient = "records")

with open("gulte_news.json", "w+") as output_file:
    output_file.write(obj)
