#!/usr/bin/python3.5 -W ignore

# Import necessary Packages
from newsplease import NewsPlease
from bs4 import BeautifulSoup
from requests import get
import requests, re
import json

#Passing Main Url Source
url = 'https://timesofindia.indiatimes.com/entertainment/telugu/movies/news'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
obj = []

for i in range(14):
	# Getting the urls of cinema news
    container = soup.find('div', class_='movienews_listn clearfix')
    container = ((container.findAll('p')[i]).a)['href']
    if 'https://timesofindia.indiatimes.com' in container:
        link = container
    else:
        link = 'https://timesofindia.indiatimes.com' + container

	# Extracting the content of the News
    article = NewsPlease.from_url(link)
    t = article.title
    dtp = str(article.date_publish)
    iu = article.image_url
    a = article.authors
    td = article.text

	# Storing News Content in json file
    obj.append({'title' : t, 'Author' : a, 'Date' : dtp, 'Image_url' : iu, 'News' : td})

with open('TOI_news.json', 'w') as outfile:
    json.dump(obj, outfile)
