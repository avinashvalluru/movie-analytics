#!/usr/bin/python3.5 -W ignore

from bs4 import BeautifulSoup as bs
from requests import get
from sys import argv
import numpy as np
import json, argparse
import pandas as pd

parser = argparse.ArgumentParser(description = 'Hero Statistics')
parser.add_argument('--Hero', help='Enter Hero Name')

obj = {}
df = pd.read_csv('./python/hero_social_links.csv')

def instaInfo(url):
    insta_data = 0
    if url is not np.nan:
        response = get(url)
        soup = bs(response.text,'html.parser')
        followers = soup.find('meta', {'name': 'description'})['content']
        insta_data = ((((followers.split('-')[0]).split(','))[0].split(' '))[0])
    return insta_data


def fbInfo(url):
    likes = 0
    if url is not np.nan:
        response = get(url)
        soup = bs(response.text,'html.parser')
        container = soup.find(class_ = '_4-u3 _5sqi _5sqk')
        likes = container.find(class_ = '_52id _50f5 _50f7').text
        likes = (str(int(int((likes.split(' ')[0]).replace(',','')) / 1000)) + 'K')

    return likes


def twitterInfo(url):
    followers = 0
    if url is not np.nan:
        response = get(url)
        soup = bs(response.text,'html.parser')
        container = soup.find(class_ = 'ProfileNav-item ProfileNav-item--followers')
        followers = container.find(class_ = 'ProfileNav-value').text
    return followers


args = parser.parse_args()
if (len(argv)>1):
    hero = argv[2]
    constraint = df['HERO'] == hero
    likes = fbInfo(list(df[constraint].Facebook)[0])
    followers = twitterInfo(list(df[constraint].Twitter)[0])
    insta_data = instaInfo(list(df[constraint].Instagram)[0])
    obj.update({"Fb_likes" : likes, "Twitter_followers" : followers, "Insta_followers" : insta_data})
    print(json.dumps({'social_media' :obj}))
