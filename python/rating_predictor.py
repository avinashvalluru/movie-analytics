#!/usr/bin/python3.5 -W ignore

from sys import argv
import pickle
import argparse
import spacy
import json
parser = argparse.ArgumentParser(description='Movie_rating prediction from plot')
parser.add_argument('--Plot', help='Enter movie plot for analysis.')
args = parser.parse_args()
regressor_model = pickle.load(open('./python/TOI_RATING.pkl', 'rb'))    
nlp = spacy.load('en')
if len(argv) > 1:
    plot_text = argv[2]
    plot = nlp(plot_text)
    rating = float(regressor_model.predict([plot.vector]))
    data = {}
    data['rating'] = rating	
    print(json.dumps(data))
else:
    print("Enter the movie_plot using arguments --plot 'YOUR_PLOT_HERE'. ")





