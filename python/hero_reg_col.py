#!/usr/bin/python3.5 -W ignore


from sys import argv
import pandas as pd
import json
import argparse

# Parse the input
parser = argparse.ArgumentParser(description = 'Hero Statistics')
parser.add_argument('--Hero', help='Enter Hero Name')
args = parser.parse_args()
if (len(argv)>1):
    hero = argv[2]

# Read excel file
df = pd.read_excel('./python//Area_business.xlsx')


# Regional-wise collections
reg_col = {}
h = df.Hero == hero
for i in range(3, 11):
    reg_col.update({df[h].columns[i] : round((df[h][df[h].columns[i]].sum() / len(df[h])), 2) })

jsonobj = {"Region_Collections" : reg_col }
print(json.dumps(jsonobj))
