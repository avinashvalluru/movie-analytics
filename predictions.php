<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Movie - Analytics| </title>

    <!-- Bootstrap -->
    <link href="./vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="./vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="./vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="./vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="./vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="./vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="./vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="./build/css/custom.min.css" rel="stylesheet">
    <link href="./build/css/main.css" rel="stylesheet">
    <style>
    .star-ratings-sprite {
        background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;
        font-size: 0;
        height: 21px;
        line-height: 0;
        overflow: hidden;
        text-indent: -999em;
        width: 110px;
        margin: 0 auto;
        }
        .star-ratings-sprite-rating {
        background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;
        background-position: 0 100%;
        float: left;
        height: 21px;
        display: block;
        }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-file-video-o"></i> <span>Movie- Analytics</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Vishnu Manchu</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="dashboard"><i class="fa fa-home"></i>Dashboard</a></li>
                  <li><a href="predictions"><i class="fa fa-line-chart"></i>Predictions</a></li>
                    <li><a href="statistics"><i class="fa fa-bar-chart"></i>Statistics</a></li>
                  <li><a href="trending-news"><i class="fa fa-newspaper-o"></i>Trending News</a></li>
                  <li><a href="youtube"><i class="fa fa-youtube-play"></i>Youtube</a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">Vishnu Manchu
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
             <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_title">
                              <h2>Story Rating Predictor<small></small></h2>
                              <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
                              <br />
                              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">StoryLine<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <textarea id="storyLine" required="required" class="form-control" name="storyLine" ></textarea>

                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                       <button type="button" id = "submit_storyLine" class="btn btn-success">Submit</button>
                                    </div>
                                 </div>
                              </form>
                              <div class="loader hide">Loading...</div>
                              <div class="text-center" id="analysis-result">

                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                         <div class="x_panel bar-graph hide">
                           <div class="x_title">
                             <h2>Budget & Gross Predictons</h2>
                             <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
                             <canvas id="budgetbarChart"></canvas>
                           </div>
                         </div>
                     </div>
                  </div>

               </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Movie-Analytics by <a href="#">SVEI</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="./vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="./vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="./vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="./vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="./vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="./vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="./vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="./vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="./vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="./vendors/Flot/jquery.flot.js"></script>
    <script src="./vendors/Flot/jquery.flot.pie.js"></script>
    <script src="./vendors/Flot/jquery.flot.time.js"></script>
    <script src="./vendors/Flot/jquery.flot.stack.js"></script>
    <script src="./vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="./vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="./vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="./vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="./vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="./vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="./vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="./vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="./vendors/moment/min/moment.min.js"></script>
    <script src="./vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="./build/js/custom.js"></script>
  <script type="text/javascript">
    $('#submit_storyLine').click(function(){
      var storyLine = $('#storyLine').val();
      if(storyLine)
      {
        $.ajax({
            type:"POST",
            url:"rating_s",
            data: { storyLine :storyLine },
            beforeSend: function(){
                  // $('.ajax-loader-wrapper').show();

                   $(".loader").removeClass("hide");
              },
              complete: function(){
                  // $('.ajax-loader-wrapper').hide();
                   $(".loader").addClass("hide");
              },
            success: function(response){
            //$("#analysis-result").html(response);
            var res = JSON.parse(response);
            console.log(res.Rating);
              $('.bar-graph').removeClass("hide");
              var rating = res.Rating
              rating = (rating/5)*100;
              $('#analysis-result').html('<h5>Predicted rating : '+res.Rating+'/5</h5><div class="star-ratings-sprite"><span style="width:'+rating+'%" class="star-ratings-sprite-rating"></span></div>')

            if ($('#budgetbarChart').length ){

                var ctx = document.getElementById("budgetbarChart");
                var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                  labels: ["Budget", "Gross"],
                  datasets: [{
                  backgroundColor: ["#26B99A" , "#26f99A"],
                  data: [res.Budget,res.Gross]
                  }]
                },

                options: {
                  scales: {
                  yAxes: [{
                    ticks: {
                    beginAtZero: true
                    }
                  }],
                  xAxes: [{
                    categoryPercentage: 1.0,
                    barPercentage: 0.3

                  }]
                  }
                }
                });

              }

            }
          });
      }else {
        alert("Enter the StoryLine")
      }
    });
  </script>
  <script type="text/javascript">

  </script>
  </body>
</html>
